package com.evolint.bluetooghle;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import android.support.v7.app.AppCompatActivity;


import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

@TargetApi(21)
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    private BluetoothAdapter mBluetoothAdapter;
    private int REQUEST_ENABLE_BT = 1;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 10000;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothGatt mGatt;
    private static BluetoothDevice dev;
    private ListView l;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private ArrayAdapter<String> adapter ;
    private ArrayList<BluetoothDevice> mDevices;
    private String[] str={"ab","cd"};
    static List<BluetoothDevice> myDevice;
    private boolean scanning=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        l=(ListView)findViewById(R.id.listView);
        mDevices=new ArrayList<BluetoothDevice>();
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        l.setOnItemClickListener(this);
        Intent service = new Intent(this,com.evolint.bluetooghle.BluetoothLeService.class);
        startService(service);




        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        //** code for testing if ble is supported*****************************************************************
        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported",
                    Toast.LENGTH_SHORT).show();
            Log.d("zubair not supported", "ble not supported");
            finish();
        }

        //** Creating a BluetoothAdapter instance to work with device's bluetooth
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////setting up toolbar and option menu/////////////////////////////////////////////////////////////
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("BLE LED app");
        getSupportActionBar().setHomeButtonEnabled(true);

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.option_menue, menu);
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == R.id.scan) {

            scanLeDevice(true);


           // adapter.notifyDataSetChanged();

        }
        if (id == R.id.discon) {
           // mGatt.disconnect();

        }


        return true;
    }






    @Override
    protected void onResume() {
        super.onResume();
        Log.d("zubair on resume", "in on resume");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //*********** Requesting bluetooth enable permission from user if disabled
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.d("zubair request", "startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);");

        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();//mLEScanner will be needed to start and stop bluetooth scanning
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)//The scan mode can be one of SCAN_MODE_LOW_POWER, SCAN_MODE_BALANCED or SCAN_MODE_LOW_LATENCY.https://developer.android.com/reference/android/bluetooth/le/ScanSettings.html#SCAN_MODE_LOW_LATENCY
                        .build();
                filters = new ArrayList<ScanFilter>();//Criteria for filtering result from Bluetooth LE scans. A ScanFilter allows clients to restrict scan results to only those that are of interest to them.
                                                       // https://developer.android.com/reference/android/bluetooth/le/ScanFilter.html
                                                       //will be required as an argument to call startScan()
                Log.d("zubair sdk>21", "in sdk>21);");
            }
           // scanLeDevice(true);
        }

        mLeDeviceListAdapter = new LeDeviceListAdapter();
        l.setAdapter(adapter);
       // setListAdapter(mLeDeviceListAdapter);
    }







    @Override
    protected void onPause() {
        Log.d("zubair onpause", "in on pause");
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            scanLeDevice(false);
        }

    }




    @Override
    protected void onDestroy() {
        Log.d("zubair ondestroy", "ondestroy");
        if (mGatt == null) {
            return;
        }
        mGatt.close();
        mGatt = null;
        super.onDestroy();
    }







    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }







    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {/// the action here will be delayed for SCAN_PERIOD. That means scan will be stopped after certain time.
                    if (Build.VERSION.SDK_INT < 21) {
                        Log.d("zubair Stopping LeScan", "Starting LeScan");
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        scanning=false;
                    } else {
                        Log.d("zubair Stopping Scan", "Starting LeScan");
                        mLEScanner.stopScan(mScanCallback);
                        scanning=false;

                    }

                }
            }, SCAN_PERIOD);
            if (Build.VERSION.SDK_INT < 21) {
                Log.d("zubair Starting LeScan", "Starting LeScan<21");
                mBluetoothAdapter.startLeScan(mLeScanCallback);
                scanning=true;
            } else {
                Log.d("zubair Starting Scan", "Starting LeScan>21");
                mLEScanner.startScan(filters, settings, mScanCallback);
                scanning=true;
            }
        } else {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                scanning=false;
            } else {
                mLEScanner.stopScan(mScanCallback);
                scanning=false;
            }
        }
    }








    private ScanCallback mScanCallback = new ScanCallback() {

        @Override
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ////******** This function will be Callback when a BLE advertisement has been found*****/////////
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d("zubair callbackType", String.valueOf(callbackType));
            Log.d("zubair result", result.toString());
            BluetoothDevice btDevice = result.getDevice();

            mLeDeviceListAdapter.addDevice(btDevice);
            if(!mDevices.contains(btDevice)) {
                mDevices.add(btDevice);
                adapter.add(btDevice.getName());
            }


            Log.d("zubair ","mdevices"+ mDevices.toString());




            //connectToDevice(btDevice);

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };








    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                ////******** This function will be called when scanning is going on by starLetScan() method*****/////////
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
            //if (device == null) return;
            dev=mDevices.get(position);
            final Intent intent = new Intent(this, com.evolint.bluetooghle.Control.class);
            intent.putExtra(com.evolint.bluetooghle.Control.EXTRAS_DEVICE_NAME, mDevices.get(position).getName());
            intent.putExtra(com.evolint.bluetooghle.Control.EXTRAS_DEVICE_ADDRESS, mDevices.get(position).getAddress());
            if (scanning) {
            //    mBluetoothAdapter.stopLeScan(mLeScanCallback);
             //   scanning = false;
            }
            startActivity(intent);

    }


    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = com.evolint.bluetooghle.MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText("Unknown Device");
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }



}